package com.example.petter.domain.repository;

import com.example.petter.domain.model.Pessoa7;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface Pessoa7Repository extends JpaRepository<Pessoa7,Long> {

    Optional<Pessoa7> findByNome(String nome);
}
