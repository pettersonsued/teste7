package com.example.petter.domain.repository;

import com.example.petter.domain.model.Pessoa6;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface Pessoa6Repository extends JpaRepository<Pessoa6,Long> {

    Optional<Pessoa6> findByNome(String nome);
}
