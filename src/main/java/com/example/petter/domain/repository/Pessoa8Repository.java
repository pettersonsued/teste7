package com.example.petter.domain.repository;

import com.example.petter.domain.model.Pessoa8;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface Pessoa8Repository extends JpaRepository<Pessoa8, Long> {

    Optional<Pessoa8> findByNome(String nome);
}
