package com.example.petter.domain.repository;

import com.example.petter.domain.model.Pessoa4;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface Pessoa4Repository extends JpaRepository<Pessoa4, Long > {

    Optional<Pessoa4> findByNome(String nome);
}
