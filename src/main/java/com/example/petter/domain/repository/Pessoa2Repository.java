package com.example.petter.domain.repository;

import com.example.petter.domain.model.Pessoa2;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface Pessoa2Repository extends JpaRepository<Pessoa2,Long> {

    Optional<Pessoa2> findByNome(String nome);
}
