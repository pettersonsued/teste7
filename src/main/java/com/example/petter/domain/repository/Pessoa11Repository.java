package com.example.petter.domain.repository;

import com.example.petter.domain.model.Pessoa11;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface Pessoa11Repository extends JpaRepository<Pessoa11,Long> {


    Optional<Pessoa11> findByNome(String nome);
}
