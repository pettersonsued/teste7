package com.example.petter.domain.repository;

import com.example.petter.domain.model.Pessoa10;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface Pessoa10Repository extends JpaRepository<Pessoa10, Long> {

    Optional<Pessoa10> findByNome(String nome);
}
