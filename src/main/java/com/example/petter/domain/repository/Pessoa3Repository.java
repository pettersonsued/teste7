package com.example.petter.domain.repository;

import com.example.petter.domain.model.Pessoa3;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface Pessoa3Repository extends JpaRepository<Pessoa3,Long> {


    Optional<Pessoa3> findByNome(String nome);
}
