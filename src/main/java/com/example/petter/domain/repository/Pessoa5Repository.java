package com.example.petter.domain.repository;

import com.example.petter.domain.model.Pessoa5;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface Pessoa5Repository extends JpaRepository<Pessoa5, Long > {

    Optional<Pessoa5> findByNome(String nome);
}
