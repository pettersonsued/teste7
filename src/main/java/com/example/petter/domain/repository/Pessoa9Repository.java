package com.example.petter.domain.repository;

import com.example.petter.domain.model.Pessoa9;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface Pessoa9Repository extends JpaRepository<Pessoa9, Long> {

    Optional<Pessoa9> findByNome(String nome);
}
