package com.example.petter.domain.service;

import com.example.petter.domain.exception.CadastroException;
import com.example.petter.domain.model.Pessoa6;
import com.example.petter.domain.repository.Pessoa6Repository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class Pessoa6Service {


    private Pessoa6Repository pessoa6Repository;


    public List<Pessoa6> listar(){
        return this.pessoa6Repository.findAll();
    }

    public Pessoa6 salvar(Pessoa6 pessoa6){
        boolean existeNome = this.pessoa6Repository.findByNome(pessoa6.getNome())
                .filter(p -> !p.equals(pessoa6)).isPresent();
        if(existeNome){
            throw new CadastroException("nome já existe");
        }
        return this.pessoa6Repository.save(pessoa6);
    }

    public Optional<Pessoa6> getPessoa6(Long pessoa6Id){
        return this.pessoa6Repository.findById(pessoa6Id);
    }

    public boolean existeId(Long pessoa6Id){
        return this.pessoa6Repository.existsById(pessoa6Id);
    }

    public void excluir(Long pessoa6Id){
        this.pessoa6Repository.deleteById(pessoa6Id);
    }
}
