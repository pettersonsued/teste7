package com.example.petter.domain.service;

import com.example.petter.domain.exception.CadastroException;
import com.example.petter.domain.model.Pessoa3;
import com.example.petter.domain.repository.Pessoa2Repository;
import com.example.petter.domain.repository.Pessoa3Repository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class Pessoa3Service {



    private Pessoa3Repository pessoa3Repository;

    public List<Pessoa3> listar(){
        return this.pessoa3Repository.findAll();
    }

    public Pessoa3 salvar(Pessoa3 pessoa3){
        boolean existeNome = this.pessoa3Repository.findByNome(pessoa3.getNome())
                .filter(p -> !p.equals(pessoa3)).isPresent();
        if(existeNome){
            throw new CadastroException("Nome já existe");
        }
        return this.pessoa3Repository.save(pessoa3);
    }

    public Optional<Pessoa3> getPessoa3(Long pessoa3Id){
        return this.pessoa3Repository.findById(pessoa3Id);
    }

    public boolean existeId(Long pessoa3Id){
        return this.pessoa3Repository.existsById(pessoa3Id);
    }

    public void excluir(Long pessoa3Id){
        this.pessoa3Repository.deleteById(pessoa3Id);
    }
}
