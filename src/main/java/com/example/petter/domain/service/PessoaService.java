package com.example.petter.domain.service;

import com.example.petter.domain.exception.CadastroException;
import com.example.petter.domain.exception.EntidadeNaoEncontradaException;
import com.example.petter.domain.model.Endereco;
import com.example.petter.domain.model.Pessoa;
import com.example.petter.domain.repository.EnderecoRepository;
import com.example.petter.domain.repository.PessoaRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class PessoaService {

    private PessoaRepository pessoaRepository;

    private EnderecoRepository enderecoRepository;

    public List<Pessoa> listar(){
        return this.pessoaRepository.findAll();
    }

    public Pessoa salvar(Pessoa pessoa){
        boolean existeNome = this.pessoaRepository.findByNome(pessoa.getNome())
                .filter(p -> !p.equals(pessoa)).isPresent();
        if(existeNome){
            throw new CadastroException("Nome já em uso");
        }
        Optional<Endereco> endereco = this.enderecoRepository.findById(pessoa.getEndereco().getId());
        if(endereco.isEmpty()){
            throw new EntidadeNaoEncontradaException("Endereco não existe");
        }
        pessoa.setEndereco(endereco.get());
        return this.pessoaRepository.save(pessoa);
    }

    public Optional<Pessoa> getPessoa(Long pessoaId){
        return this.pessoaRepository.findById(pessoaId);
    }

    public boolean existeId(Long pessoaId){
        return this.pessoaRepository.existsById(pessoaId);
    }

    public void excluir(Long pessoaId){
        this.pessoaRepository.deleteById(pessoaId);
    }
}
