package com.example.petter.domain.service;

import com.example.petter.domain.model.Endereco;
import com.example.petter.domain.repository.EnderecoRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class EnderecoService {


    private EnderecoRepository enderecoRepository;



    public List<Endereco> listar(){
        return this.enderecoRepository.findAll();
    }

    public Endereco salvar(Endereco endereco){
        return this.enderecoRepository.save(endereco);
    }

    public Optional<Endereco> getEndereco(Long enderecoId){
        return this.enderecoRepository.findById(enderecoId);
    }

    public boolean existeId(Long enderecoId){
        return this.enderecoRepository.existsById(enderecoId);
    }

    public void excluir(Long enderecoId){
        this.enderecoRepository.deleteById(enderecoId);
    }
}
