package com.example.petter.domain.service;

import com.example.petter.domain.exception.CadastroException;
import com.example.petter.domain.model.Pessoa11;
import com.example.petter.domain.repository.Pessoa11Repository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class Pessoa11Service {

    private Pessoa11Repository pessoa11Repository;



    public List<Pessoa11> listar(){
        return this.pessoa11Repository.findAll();
    }

    public Pessoa11 salvar(Pessoa11 pessoa11){
        boolean existeNome = this.pessoa11Repository.findByNome(pessoa11.getNome())
                .filter(p -> !p.equals(pessoa11)).isPresent();
        if(existeNome){
            throw new CadastroException("nome já existe");
        }
        return this.pessoa11Repository.save(pessoa11);
    }

    public Optional<Pessoa11> getPessoa11(Long pesssoa11Id){
        return this.pessoa11Repository.findById(pesssoa11Id);
    }

    public boolean existeId(Long pessoa11Id){
        return this.pessoa11Repository.existsById(pessoa11Id);
    }

    public void excluir(Long pessoa11Id){
        this.pessoa11Repository.deleteById(pessoa11Id);
    }
}
