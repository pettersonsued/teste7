package com.example.petter.domain.service;

import com.example.petter.domain.exception.CadastroException;
import com.example.petter.domain.model.Pessoa9;
import com.example.petter.domain.repository.Pessoa9Repository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class Pessoa9Service {


    private Pessoa9Repository pessoa9Repository;



    public List<Pessoa9> listar(){
        return this.pessoa9Repository.findAll();
    }

    public Pessoa9 salvar(Pessoa9 pessoa9){
        boolean existeNome = this.pessoa9Repository.findByNome(pessoa9.getNome())
                .filter(p -> !p.equals(pessoa9)).isPresent();
        if(existeNome){
            throw new CadastroException("nome já existe");
        }
        return this.pessoa9Repository.save(pessoa9);
    }

    public Optional<Pessoa9> getPessoa9(Long pessoa9Id){
        return this.pessoa9Repository.findById(pessoa9Id);
    }


    public boolean existeId(Long pessoa9Id){
        return this.pessoa9Repository.existsById(pessoa9Id);
    }

    public void excluir(Long pessoa9Id){
        this.pessoa9Repository.deleteById(pessoa9Id);
    }
}
