package com.example.petter.domain.service;

import com.example.petter.domain.exception.CadastroException;
import com.example.petter.domain.model.Pessoa4;
import com.example.petter.domain.repository.Pessoa4Repository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class Pessoa4Service {


    private Pessoa4Repository pessoa4Repository;


    public List<Pessoa4> listar(){
        return this.pessoa4Repository.findAll();
    }

    public Pessoa4 salvar(Pessoa4 pessoa4){
        boolean existeNome =this.pessoa4Repository.findByNome(pessoa4.getNome())
                .filter(p -> !p.equals(pessoa4)).isPresent();
        if(existeNome){
            throw new CadastroException("nome já existe");
        }
        return this.pessoa4Repository.save(pessoa4);
    }

    public Optional<Pessoa4> getPessoa4(Long pessoa4Id){
        return this.pessoa4Repository.findById(pessoa4Id);
    }


    public boolean existeId(Long pessoa4Id){
       return this.pessoa4Repository.existsById(pessoa4Id);
    }

    public void excluir(Long pessoa4Id){
        this.pessoa4Repository.deleteById(pessoa4Id);
    }
}
