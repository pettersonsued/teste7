package com.example.petter.domain.service;

import com.example.petter.domain.exception.CadastroException;
import com.example.petter.domain.model.Pessoa8;
import com.example.petter.domain.repository.Pessoa8Repository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class Pessoa8Service {



    private Pessoa8Repository pessoa8Repository;



    public List<Pessoa8> listar(){
        return this.pessoa8Repository.findAll();
    }

    public Pessoa8 salvar(Pessoa8 pessoa8){
        boolean existeNome = this.pessoa8Repository.findByNome(pessoa8.getNome())
                .filter(p -> !p.equals(pessoa8)).isPresent();
        if(existeNome){
            throw new CadastroException("nome já existe");
        }
        return this.pessoa8Repository.save(pessoa8);
    }


    public Optional<Pessoa8> getPessoa8(Long pessoa8Id){
        return this.pessoa8Repository.findById(pessoa8Id);
    }

    public boolean existeId(Long pessoa8Id){
        return this.pessoa8Repository.existsById(pessoa8Id);
    }

    public void excluir(Long pesoa8Id){
        this.pessoa8Repository.deleteById(pesoa8Id);
    }
}
