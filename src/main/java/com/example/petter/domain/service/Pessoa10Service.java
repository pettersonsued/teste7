package com.example.petter.domain.service;

import com.example.petter.domain.exception.CadastroException;
import com.example.petter.domain.model.Pessoa10;
import com.example.petter.domain.repository.Pessoa10Repository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class Pessoa10Service {


    private Pessoa10Repository pessoa10Repository;


    public List<Pessoa10> listar(){
        return this.pessoa10Repository.findAll();
    }

    public Pessoa10 salvar(Pessoa10 pessoa10){
        boolean existeNome = this.pessoa10Repository.findByNome(pessoa10.getNome())
                .filter(p -> !p.equals(pessoa10)).isPresent();
        if(existeNome){
            throw new CadastroException("nome já existe");
        }
        return this.pessoa10Repository.save(pessoa10);
    }

    public Optional<Pessoa10> getPessoa10(Long pesssoa10Id){
        return this.pessoa10Repository.findById(pesssoa10Id);
    }

    public boolean existeId(Long pessoa10Id){
        return this.pessoa10Repository.existsById(pessoa10Id);
    }

    public void excluir(Long pessoa10Id){
        this.pessoa10Repository.deleteById(pessoa10Id);
    }
}
