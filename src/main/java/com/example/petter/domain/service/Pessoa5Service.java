package com.example.petter.domain.service;

import com.example.petter.domain.exception.CadastroException;
import com.example.petter.domain.model.Pessoa5;
import com.example.petter.domain.repository.Pessoa5Repository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class Pessoa5Service {


    private Pessoa5Repository pessoa5Repository;



    public List<Pessoa5> listar(){
        return this.pessoa5Repository.findAll();
    }

    public Pessoa5 salvar(Pessoa5 pessoa5){
        boolean existeNome=this.pessoa5Repository.findByNome(pessoa5.getNome())
                .filter(p -> !p.equals(pessoa5)).isPresent();
        if(existeNome){
            throw new CadastroException("nome já existe");
        }
        return this.pessoa5Repository.save(pessoa5);
    }

    public Optional<Pessoa5> getPessoa5(Long pessoa5Id){
        return this.pessoa5Repository.findById(pessoa5Id);
    }

    public boolean existeId(Long pessoa5Id){
        return this.pessoa5Repository.existsById(pessoa5Id);
    }

    public void excluir(Long pessoa5Id){
       this.pessoa5Repository.deleteById(pessoa5Id);
    }
}
