package com.example.petter.domain.service;

import com.example.petter.domain.exception.CadastroException;
import com.example.petter.domain.model.Pessoa7;
import com.example.petter.domain.repository.Pessoa7Repository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class Pessoa7Service {




    private Pessoa7Repository pessoa7Repository;


    public List<Pessoa7> listar(){
        return this.pessoa7Repository.findAll();
    }

    public Pessoa7 salvar(Pessoa7 pessoa7){
        boolean existeNome = this.pessoa7Repository.findByNome(pessoa7.getNome())
                .filter(p -> !p.equals(pessoa7)).isPresent();
        if(existeNome){
            throw new CadastroException("nome já existe");
        }
        return this.pessoa7Repository.save(pessoa7);
    }

    public Optional<Pessoa7> getPessoa7(Long pessoa7Id){
        return this.pessoa7Repository.findById(pessoa7Id);
    }

    public boolean existeId(Long pessoa7Id){
        return this.pessoa7Repository.existsById(pessoa7Id);
    }

    public void excluir(Long pesssoa7Id){
        this.pessoa7Repository.deleteById(pesssoa7Id);
    }
}
