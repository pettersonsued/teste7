package com.example.petter.domain.service;

import com.example.petter.domain.exception.CadastroException;
import com.example.petter.domain.model.Pessoa2;
import com.example.petter.domain.repository.Pessoa2Repository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class Pessoa2Service {


    private Pessoa2Repository pessoa2Repository;


    public List<Pessoa2> listar(){
        return this.pessoa2Repository.findAll();
    }


    public Pessoa2 salvar(Pessoa2 pessoa2){
        boolean existeNome = this.pessoa2Repository.findByNome(pessoa2.getNome())
                .filter(p -> !p.equals(pessoa2)).isPresent();
        if(existeNome){
            throw new CadastroException("nome já existe");
        }
        return this.pessoa2Repository.save(pessoa2);
    }

    public Optional<Pessoa2> getPessoa2(Long pessoa2Id){
        return this.pessoa2Repository.findById(pessoa2Id);
    }

    public boolean existeId(Long pessoa2Id){
        return  this.pessoa2Repository.existsById(pessoa2Id);
    }

    public void excluir(Long pessoa2Id){
        this.pessoa2Repository.deleteById(pessoa2Id);
    }
}
