package com.example.petter.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Getter
@Setter
@Entity
public class Endereco {

    @EqualsAndHashCode.Include
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String logradouro;

    private String cep;

    private String cidade;

    private String estado;

    private int numero;

    private String status;

    @JsonIgnore
    @OneToMany(mappedBy = "endereco", cascade = CascadeType.ALL)
    List<Pessoa> pessoas;
}
