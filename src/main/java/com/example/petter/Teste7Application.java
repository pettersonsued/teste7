package com.example.petter;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@OpenAPIDefinition(info = @Info(title = "Swagger OpeApi", version = "1", description = "API desenvolvida para User"))
public class Teste7Application {

	public static void main(String[] args) {
		SpringApplication.run(Teste7Application.class, args);
	}

}
