package com.example.petter.api.controller;

import com.example.petter.domain.model.Pessoa2;
import com.example.petter.domain.service.Pessoa2Service;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@RestController
@RequestMapping(value = "pessoa2s", produces = {"application/json"})
public class Pessoa2Controller {

    private Pessoa2Service pessoa2Service;



    @GetMapping
    public List<Pessoa2> listar(){
        return this.pessoa2Service.listar();
    }

    @Operation(summary = "realiza a busca de uma Pessoa por Id", method = "GET")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Pessoa encontrado com sucesso"),
            @ApiResponse(responseCode = "422", description = "Dados de requisição inválida"),
            @ApiResponse(responseCode = "400", description = "Parametros inválidos"),
            @ApiResponse(responseCode = "500", description = "Erro no servidor"),
    })
    @GetMapping("/{pessoa2Id}")
    public ResponseEntity<Pessoa2> buscar(@PathVariable Long pessoa2Id){
        Optional<Pessoa2> pessoa2 = this.pessoa2Service.getPessoa2(pessoa2Id);
        if(pessoa2.isPresent()){
            return ResponseEntity.ok(pessoa2.get());
        }
        return ResponseEntity.notFound().build();
    }

    @Operation(summary = "realiza a adição de um nova Pessoa", method = "POST")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Pessoa cadastrado com sucesso"),
            @ApiResponse(responseCode = "422", description = "Dados de requisição inválida"),
            @ApiResponse(responseCode = "400", description = "Parametros inválidos"),
            @ApiResponse(responseCode = "500", description = "Erro no servidor"),
    })
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public Pessoa2 adicionar(@Valid @RequestBody Pessoa2 pessoa2){
        return this.pessoa2Service.salvar(pessoa2);
    }

    @Operation(summary = "realiza a atualização de uma Pessoa", method = "PUT")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Pessoa Atualizada com sucesso"),
            @ApiResponse(responseCode = "422", description = "Dados de requisição inválida"),
            @ApiResponse(responseCode = "400", description = "Parametros inválidos"),
            @ApiResponse(responseCode = "500", description = "Erro no servidor"),
    })
    @PutMapping("/{pessoa2Id}")
    public ResponseEntity<Pessoa2> atualizar(@PathVariable Long pessoa2Id, @Valid @RequestBody Pessoa2 pessoa2){
        if(!this.pessoa2Service.existeId(pessoa2Id)){
            return ResponseEntity.notFound().build();
        }
        pessoa2.setId(pessoa2Id);
        pessoa2 = this.pessoa2Service.salvar(pessoa2);
        return ResponseEntity.ok(pessoa2);
    }

    @Operation(summary = "realiza a exclusão de uma Pessoa", method = "DELETE")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Pessoa Deletada com sucesso"),
            @ApiResponse(responseCode = "422", description = "Dados de requisição inválida"),
            @ApiResponse(responseCode = "400", description = "Parametros inválidos"),
            @ApiResponse(responseCode = "500", description = "Erro no servidor"),
    })
    @DeleteMapping("/{pessoa2Id}")
    public ResponseEntity<Void> excluir(@PathVariable Long pessoa2Id){
        if(!this.pessoa2Service.existeId(pessoa2Id)){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.noContent().build();
    }
}
