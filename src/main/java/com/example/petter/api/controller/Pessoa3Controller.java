package com.example.petter.api.controller;

import com.example.petter.domain.model.Pessoa3;
import com.example.petter.domain.service.Pessoa3Service;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@RestController
@RequestMapping(value = "pessoa3s", produces = {"application/json"})
public class Pessoa3Controller {



    private Pessoa3Service pessoa3Service;

    @GetMapping
    public List<Pessoa3> listar(){
        return this.pessoa3Service.listar();
    }

    @Operation(summary = "realiza a busca de uma Pessoa por Id", method = "GET")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Pessoa encontrado com sucesso"),
            @ApiResponse(responseCode = "422", description = "Dados de requisição inválida"),
            @ApiResponse(responseCode = "400", description = "Parametros inválidos"),
            @ApiResponse(responseCode = "500", description = "Erro no servidor"),
    })
    @GetMapping("/{pessoa3Id}")
    public ResponseEntity<Pessoa3> buscar(@PathVariable Long pessoa3Id){
        Optional<Pessoa3> pessoa = this.pessoa3Service.getPessoa3(pessoa3Id);
        if(pessoa.isPresent()){
            return ResponseEntity.ok(pessoa.get());
        }
        return ResponseEntity.notFound().build();
    }

    @Operation(summary = "realiza a adição de um nova Pessoa", method = "POST")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Pessoa cadastrado com sucesso"),
            @ApiResponse(responseCode = "422", description = "Dados de requisição inválida"),
            @ApiResponse(responseCode = "400", description = "Parametros inválidos"),
            @ApiResponse(responseCode = "500", description = "Erro no servidor"),
    })
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public Pessoa3 adicionar(@Valid @RequestBody Pessoa3 peswsoa3){
        return this.pessoa3Service.salvar(peswsoa3);
    }

    @Operation(summary = "realiza a atualização de uma Pessoa", method = "PUT")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Pessoa Atualizada com suecesso"),
            @ApiResponse(responseCode = "422", description = "Dados de requisição inválida"),
            @ApiResponse(responseCode = "400", description = "Parametros inválidos"),
            @ApiResponse(responseCode = "500", description = "Erro no servidor"),
    })
    @PutMapping("/{pessoa3Id}")
    public ResponseEntity<Pessoa3> atualizar(@PathVariable Long pessoa3Id, @Valid @RequestBody Pessoa3 pessoa3){
        if(!this.pessoa3Service.existeId(pessoa3Id)){
            return ResponseEntity.notFound().build();
        }
        pessoa3.setId(pessoa3Id);
        pessoa3 = this.pessoa3Service.salvar(pessoa3);
        return ResponseEntity.ok(pessoa3);
    }

    @Operation(summary = "realiza a exclusão de uma Pessoa", method = "DELETE")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Pessoa Deletada com sucesso"),
            @ApiResponse(responseCode = "422", description = "Dados de requisição inválida"),
            @ApiResponse(responseCode = "400", description = "Parametros inválidos"),
            @ApiResponse(responseCode = "500", description = "Erro no servidor"),
    })
    @DeleteMapping("/{pessoa3Id}")
    public ResponseEntity<Void> excluir(@PathVariable Long pessoa3Id){
        if(!this.pessoa3Service.existeId(pessoa3Id)){
            return ResponseEntity.notFound().build();
        }
        this.pessoa3Service.excluir(pessoa3Id);
        return ResponseEntity.noContent().build();
    }
}
