package com.example.petter.api.controller;

import com.example.petter.domain.model.Pessoa5;
import com.example.petter.domain.service.Pessoa5Service;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@RestController
@RequestMapping(value = "pessoa5s", produces = {"application/json"})
public class Pessoa5Controller {

    private Pessoa5Service pessoa5Service;

    @GetMapping
    public List<Pessoa5> listar(){
        return this.pessoa5Service.listar();
    }

    @GetMapping("/{pessoa5Id}")
    public ResponseEntity<Pessoa5> buscar(@PathVariable Long pessoa5Id){
        Optional<Pessoa5> pessoa5 = this.pessoa5Service.getPessoa5(pessoa5Id);
        if(pessoa5.isPresent()){
            return ResponseEntity.ok(pessoa5.get());
        }
        return ResponseEntity.notFound().build();
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public Pessoa5 adiconar(@Valid @RequestBody Pessoa5 pessoa5){
        return this.pessoa5Service.salvar(pessoa5);
    }

    @PutMapping("/{pessoa5Id}")
    public ResponseEntity<Pessoa5> atualizar(@PathVariable Long pessoa5Id, @Valid @RequestBody Pessoa5 pessoa5){
        if(!this.pessoa5Service.existeId(pessoa5Id)){
            return ResponseEntity.notFound().build();
        }
        pessoa5.setId(pessoa5Id);
        pessoa5 = this.pessoa5Service.salvar(pessoa5);
        return ResponseEntity.ok(pessoa5);
    }

    @DeleteMapping("/{pessoa5Id}")
    public ResponseEntity<Void> excluir(@PathVariable Long pessoa5Id){
        if(!this.pessoa5Service.existeId(pessoa5Id)){
            return ResponseEntity.notFound().build();
        }
        this.pessoa5Service.excluir(pessoa5Id);
        return ResponseEntity.noContent().build();
    }


}
