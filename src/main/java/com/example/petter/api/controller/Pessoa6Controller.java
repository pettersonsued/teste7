package com.example.petter.api.controller;

import com.example.petter.domain.model.Pessoa6;
import com.example.petter.domain.service.Pessoa6Service;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@RestController
@RequestMapping(value = "pessoa6s", produces = {"application/json"})
public class Pessoa6Controller {


    private Pessoa6Service pessoa6Service;

    @GetMapping
    public List<Pessoa6> listar(){
        return this.pessoa6Service.listar();
    }

    @Operation(summary = "realiza a busca de uma Pessoa por Id", method = "GET")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Pessoa encontrado"),
            @ApiResponse(responseCode = "422", description = "Dados de requisição inválida"),
            @ApiResponse(responseCode = "400", description = "Parametros inválidos"),
            @ApiResponse(responseCode = "500", description = "Erro no servidor"),
    })
    @GetMapping("/{pessoa6Id}")
    public ResponseEntity<Pessoa6> buscar(@PathVariable Long pessoa6Id){
        Optional<Pessoa6> pessoa6 =this.pessoa6Service.getPessoa6(pessoa6Id);
        if(pessoa6.isPresent()){
            return ResponseEntity.ok(pessoa6.get());
        }
        return ResponseEntity.notFound().build();
    }

    @Operation(summary = "realiza a adição de um nova Pessoa", method = "POST")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Pessoa cadastrado com sucesso"),
            @ApiResponse(responseCode = "422", description = "Dados de requisição inválida"),
            @ApiResponse(responseCode = "400", description = "Parametros inválidos"),
            @ApiResponse(responseCode = "500", description = "Erro no servidor"),
    })
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public Pessoa6 adicionar(@Valid @RequestBody Pessoa6 pessoa6){
        return this.pessoa6Service.salvar(pessoa6);
    }

    @Operation(summary = "realiza a atualização de uma Pessoa", method = "PUT")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Pessoa Atualizada"),
            @ApiResponse(responseCode = "422", description = "Dados de requisição inválida"),
            @ApiResponse(responseCode = "400", description = "Parametros inválidos"),
            @ApiResponse(responseCode = "500", description = "Erro no servidor"),
    })
    @PutMapping("/{pessoa6Id}")
    public ResponseEntity<Pessoa6> atualizar(@PathVariable Long pessoa6Id, @Valid @RequestBody Pessoa6 pessoa6){
        if(!this.pessoa6Service.existeId(pessoa6Id)){
            return ResponseEntity.notFound().build();
        }
        pessoa6.setId(pessoa6Id);
        pessoa6 = this.pessoa6Service.salvar(pessoa6);
        return ResponseEntity.ok(pessoa6);
    }

    @Operation(summary = "realiza a exclusão de uma Pessoa", method = "DELETE")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Pessoa Deletada com sucesso"),
            @ApiResponse(responseCode = "422", description = "Dados de requisição inválida"),
            @ApiResponse(responseCode = "400", description = "Parametros inválidos"),
            @ApiResponse(responseCode = "500", description = "Erro no servidor"),
    })
    @DeleteMapping("/{pessoa6Id}")
    public ResponseEntity<Void> excluir(@PathVariable Long pessoa6Id){
        if(!this.pessoa6Service.existeId(pessoa6Id)){
            return ResponseEntity.notFound().build();
        }
        this.pessoa6Service.excluir(pessoa6Id);
        return ResponseEntity.noContent().build();
    }
}
