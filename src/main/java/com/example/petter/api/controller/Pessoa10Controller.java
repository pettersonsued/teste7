package com.example.petter.api.controller;

import com.example.petter.domain.model.Pessoa10;
import com.example.petter.domain.service.Pessoa10Service;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@RestController
@RequestMapping(value = "pessoa10s", produces = {"application/json"})
public class Pessoa10Controller {


    private Pessoa10Service pessoa10Service;


    @GetMapping
    public List<Pessoa10> listar(){
        return this.pessoa10Service.listar();
    }

    @GetMapping("/{pessoa10Id}")
    public ResponseEntity<Pessoa10> buscar(@PathVariable Long pessoa10Id){
        Optional<Pessoa10> pessoa10 = this.pessoa10Service.getPessoa10(pessoa10Id);
        if(pessoa10.isPresent()){
            return ResponseEntity.ok(pessoa10.get());
        }
        return ResponseEntity.notFound().build();
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public Pessoa10 adicionar(@Valid @RequestBody Pessoa10 pessoa10){
        return this.pessoa10Service.salvar(pessoa10);
    }

    @PutMapping("/{pessoa10Id}")
    public ResponseEntity<Pessoa10> atualizar(@PathVariable Long pessoa10Id, @Valid @RequestBody Pessoa10 pessoa10){
        if(!this.pessoa10Service.existeId(pessoa10Id)){
            return ResponseEntity.notFound().build();
        }
        pessoa10.setId(pessoa10Id);
        pessoa10 = this.pessoa10Service.salvar(pessoa10);
        return ResponseEntity.ok(pessoa10);
    }

    @DeleteMapping("/{pessoa10Id}")
    public ResponseEntity<Void> excluir(@PathVariable Long pessoa10Id){
        if(!this.pessoa10Service.existeId(pessoa10Id)){
            return ResponseEntity.notFound().build();
        }
        this.pessoa10Service.excluir(pessoa10Id);
        return ResponseEntity.noContent().build();
    }
}
