package com.example.petter.api.controller;

import com.example.petter.domain.model.Pessoa11;
import com.example.petter.domain.service.Pessoa11Service;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@RestController
@RequestMapping(value = "pessoa11s", produces = {"application/json"})
public class Pessoa11Controller {



    private Pessoa11Service pessoa11Service;



    @GetMapping
    public List<Pessoa11> listar(){
        return this.pessoa11Service.listar();
    }


    @GetMapping("/{pessoa11Id}")
    public ResponseEntity<Pessoa11> buscar(@PathVariable Long pessoa11Id){
        Optional<Pessoa11>  pessoa11 =this.pessoa11Service.getPessoa11(pessoa11Id);
        if(pessoa11.isPresent()){
            return ResponseEntity.ok(pessoa11.get());
        }
        return ResponseEntity.notFound().build();
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public Pessoa11 adicionar(@Valid @RequestBody Pessoa11 pessoa11){
        return this.pessoa11Service.salvar(pessoa11);
    }

    @PutMapping("/{pessoa11Id}")
    public ResponseEntity<Pessoa11> atualizar(@PathVariable Long pessoa11Id, @Valid @RequestBody Pessoa11 pessoa11){
        if(!this.pessoa11Service.existeId(pessoa11Id)){
            return ResponseEntity.notFound().build();
        }
        pessoa11.setId(pessoa11Id);
        pessoa11 = this.pessoa11Service.salvar(pessoa11);
        return ResponseEntity.ok(pessoa11);
    }

    @DeleteMapping("/{pessoa11Id}")
    public ResponseEntity<Void> excluir(@PathVariable Long pessoa11Id){
        if(!this.pessoa11Service.existeId(pessoa11Id)){
            return ResponseEntity.notFound().build();
        }
        this.pessoa11Service.excluir(pessoa11Id);
        return ResponseEntity.noContent().build();
    }
}
