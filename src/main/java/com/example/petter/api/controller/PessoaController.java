package com.example.petter.api.controller;

import com.example.petter.domain.model.Pessoa;
import com.example.petter.domain.service.PessoaService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@RestController
@RequestMapping("pessoas")
public class PessoaController {


    private PessoaService pessoaService;


    @Operation(summary = "realiza a listagem de todas as Pessoas", method = "POST")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Usuário encontrado"),
            @ApiResponse(responseCode = "422", description = "Dados de requisição inválida"),
            @ApiResponse(responseCode = "400", description = "Parametros inválidos"),
            @ApiResponse(responseCode = "404", description = "Parametros inválidos"),
            @ApiResponse(responseCode = "500", description = "Erro no servidor"),
    })
    @GetMapping
    public List<Pessoa> listar(){
        return this.pessoaService.listar();
    }


    @Operation(summary = "realiza a busca de um Pessoa por Id", method = "POST")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Usuário encontrado"),
            @ApiResponse(responseCode = "422", description = "Dados de requisição inválida"),
            @ApiResponse(responseCode = "400", description = "Parametros inválidos"),
            @ApiResponse(responseCode = "404", description = "Parametros inválidos"),
            @ApiResponse(responseCode = "500", description = "Erro no servidor"),
    })
    @GetMapping("/{pessoaId}")
    public ResponseEntity<Pessoa> buscar(@PathVariable Long pessoaId){
        Optional<Pessoa> pessoa = this.pessoaService.getPessoa(pessoaId);
        if(pessoa.isPresent()){
            return ResponseEntity.ok(pessoa.get());
        }
        return ResponseEntity.notFound().build();
    }

    @Operation(summary = "realiza o cadastro de uma Pessoa", method = "POST")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Usuário Cadastrado com sucesso"),
            @ApiResponse(responseCode = "422", description = "Dados de requisição inválida"),
            @ApiResponse(responseCode = "400", description = "Parametros inválidos"),
            @ApiResponse(responseCode = "404", description = "Parametros inválidos"),
            @ApiResponse(responseCode = "500", description = "Erro no servidor"),
    })
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public Pessoa adicionar(@RequestBody Pessoa pessoa){
        return this.pessoaService.salvar(pessoa);
    }

    @Operation(summary = "realiza a atualização de uma Pessoa", method = "POST")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Usuário encontrado"),
            @ApiResponse(responseCode = "422", description = "Dados de requisição inválida"),
            @ApiResponse(responseCode = "400", description = "Parametros inválidos"),
            @ApiResponse(responseCode = "404", description = "Parametros inválidos"),
            @ApiResponse(responseCode = "500", description = "Erro no servidor"),
    })
    @PutMapping("/{pessoaId}")
    public ResponseEntity<Pessoa> atualizar(@PathVariable Long pessoaId, @RequestBody Pessoa pessoa){
        if(!this.pessoaService.existeId(pessoaId)){
            return ResponseEntity.notFound().build();
        }
        pessoa.setId(pessoaId);
        pessoa = this.pessoaService.salvar(pessoa);
        return ResponseEntity.ok(pessoa);
    }

    @Operation(summary = "realiza a exclusão de uma Pessoa", method = "POST")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Usuário encontrado"),
            @ApiResponse(responseCode = "422", description = "Dados de requisição inválida"),
            @ApiResponse(responseCode = "400", description = "Parametros inválidos"),
            @ApiResponse(responseCode = "404", description = "Parametros inválidos"),
            @ApiResponse(responseCode = "500", description = "Erro no servidor"),
    })
    @DeleteMapping("/{pessoaId}")
    public ResponseEntity<Void> excluir(@PathVariable Long pessoaId){
        if(!this.pessoaService.existeId(pessoaId)){
            return ResponseEntity.notFound().build();
        }
        this.pessoaService.excluir(pessoaId);
        return ResponseEntity.noContent().build();
    }
}
