package com.example.petter.api.controller;

import com.example.petter.domain.model.Pessoa4;
import com.example.petter.domain.service.Pessoa4Service;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@RestController
@RequestMapping(value = "pessoa4s", produces = {"application/json"})
public class Pessoa4Controller {

    private Pessoa4Service pessoa4Service;



    @GetMapping
    public List<Pessoa4> listar(){
        return this.pessoa4Service.listar();
    }

    @Operation(summary = "realiza a busca de uma Pessoa por Id", method = "GET")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Pessoa encontrado com sucesso"),
            @ApiResponse(responseCode = "422", description = "Dados de requisição inválida"),
            @ApiResponse(responseCode = "400", description = "Parametros inválidos"),
            @ApiResponse(responseCode = "500", description = "Erro no servidor problema"),
    })
    @GetMapping("/{pessoa4Id}")
    public ResponseEntity<Pessoa4> buscar(@PathVariable Long pessoa4Id){
        Optional<Pessoa4> pessoa = this.pessoa4Service.getPessoa4(pessoa4Id);
        if(pessoa.isPresent()){
            return ResponseEntity.ok(pessoa.get());
        }
        return ResponseEntity.notFound().build();
    }


    @Operation(summary = "realiza a adição de um nova Pessoa", method = "POST")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Pessoa cadastrado com sucesso"),
            @ApiResponse(responseCode = "422", description = "Dados de requisição inválida"),
            @ApiResponse(responseCode = "400", description = "Parametros inválidos"),
            @ApiResponse(responseCode = "500", description = "Erro no servidor"),
    })
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public Pessoa4 adicionar(@Valid @RequestBody Pessoa4 pessoa4){
        return this.pessoa4Service.salvar(pessoa4);
    }

    @Operation(summary = "realiza a atualização de uma Pessoa", method = "PUT")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Pessoa Atualizada com sucesso"),
            @ApiResponse(responseCode = "422", description = "Dados de requisição inválida"),
            @ApiResponse(responseCode = "400", description = "Parametros inválidos"),
            @ApiResponse(responseCode = "500", description = "Erro no servidor"),
    })
    @PutMapping("/{pessoa4Id}")
    public ResponseEntity<Pessoa4> atualizar(@PathVariable Long pessoa4Id, @Valid @RequestBody Pessoa4 pessoa4){
        if(!this.pessoa4Service.existeId(pessoa4Id)){
            return ResponseEntity.notFound().build();
        }
        pessoa4.setId(pessoa4Id);
        pessoa4 = this.pessoa4Service.salvar(pessoa4);
        return ResponseEntity.ok(pessoa4);
    }

    @Operation(summary = "realiza a exclusão de uma Pessoa", method = "DELETE")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Pessoa Deletada com sucesso"),
            @ApiResponse(responseCode = "422", description = "Dados de requisição inválida teste novamente"),
            @ApiResponse(responseCode = "400", description = "Parametros inválidos"),
            @ApiResponse(responseCode = "500", description = "Erro no servidor teste novamente"),
    })
    @DeleteMapping("/{pessoa4Id}")
    public ResponseEntity<Void> excluir(@PathVariable Long pessoa4Id){
        if(!this.pessoa4Service.existeId(pessoa4Id)){
            return ResponseEntity.notFound().build();
        }
        this.pessoa4Service.excluir(pessoa4Id);
        return ResponseEntity.noContent().build();
    }

}
