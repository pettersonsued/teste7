package com.example.petter.api.controller;

import com.example.petter.domain.model.Pessoa9;
import com.example.petter.domain.service.Pessoa9Service;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@RestController
@RequestMapping(value = "pessoa9s", produces = {"application/json"})
public class Pessoa9Controller {



    private Pessoa9Service pessoa9Service;


    @GetMapping
    public List<Pessoa9> listar(){
        return this.pessoa9Service.listar();
    }

    @GetMapping("/{pessoa9Id}")
    public ResponseEntity<Pessoa9> buscar(@PathVariable Long pessoa9Id){
        Optional<Pessoa9> pessoa9 = this.pessoa9Service.getPessoa9(pessoa9Id);
        if(pessoa9.isPresent()){
            return ResponseEntity.ok(pessoa9.get());
        }
        return ResponseEntity.notFound().build();
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public Pessoa9 aidcionar(@Valid @RequestBody Pessoa9 pessoa9){
        return this.pessoa9Service.salvar(pessoa9);
    }

    @PutMapping("/{pessoa9Id}")
    public ResponseEntity<Pessoa9> atualizar(@PathVariable Long pessoa9Id, @Valid @RequestBody Pessoa9 pessoa9){
        if(!this.pessoa9Service.existeId(pessoa9Id)){
            return ResponseEntity.notFound().build();
        }
        pessoa9.setId(pessoa9Id);
        pessoa9 = this.pessoa9Service.salvar(pessoa9);
        return ResponseEntity.ok(pessoa9);
    }

    @DeleteMapping("/{pessoa9Id}")
    public ResponseEntity<Void> excluir(@PathVariable Long pessoa9Id){
         if(!this.pessoa9Service.existeId(pessoa9Id)){
             return ResponseEntity.notFound().build();
         }
         this.pessoa9Service.excluir(pessoa9Id);
         return ResponseEntity.noContent().build();
    }
}
