package com.example.petter.api.controller;

import com.example.petter.domain.model.Pessoa7;
import com.example.petter.domain.service.Pessoa7Service;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@RestController
@RequestMapping(value = "pessoa7s", produces = {"application/json"})
public class Pessoa7Controller {


    private Pessoa7Service pessoa7Service;


    @GetMapping
    public List<Pessoa7> listar(){
        return this.pessoa7Service.listar();
    }

    @GetMapping("/{pessoa7Id}")
    public ResponseEntity<Pessoa7> buscar(@PathVariable Long pessoa7Id){
        Optional<Pessoa7> pessoa7 = this.pessoa7Service.getPessoa7(pessoa7Id);
        if(pessoa7.isPresent()){
            return ResponseEntity.ok(pessoa7.get());
        }
        return ResponseEntity.notFound().build();
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public Pessoa7 adicionar(@Valid @RequestBody Pessoa7 pessoa7){
        return this.pessoa7Service.salvar(pessoa7);
    }

    @PutMapping("/{pessoa7Id}")
    public ResponseEntity<Pessoa7> atualizar(@PathVariable Long pessoa7Id, @Valid @RequestBody Pessoa7 pessoa7){
        if(!this.pessoa7Service.existeId(pessoa7Id)){
            return ResponseEntity.notFound().build();
        }
        pessoa7.setId(pessoa7Id);
        pessoa7 = this.pessoa7Service.salvar(pessoa7);
        return ResponseEntity.ok(pessoa7);
    }

    @DeleteMapping("/{pessoa7Id}")
    public ResponseEntity<Void> excluir(@PathVariable Long pessoa7Id){
        if(!this.pessoa7Service.existeId(pessoa7Id)){
            return ResponseEntity.notFound().build();
        }
        this.pessoa7Service.excluir(pessoa7Id);
        return ResponseEntity.noContent().build();
    }
}
