package com.example.petter.api.controller;

import com.example.petter.domain.model.Pessoa8;
import com.example.petter.domain.service.Pessoa8Service;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@RestController
@RequestMapping(value = "pessoa8s", produces = {"application/json"})
public class Pessoa8Controller {


    private Pessoa8Service pessoa8Service;


    @GetMapping
    public List<Pessoa8> listar(){
        return this.pessoa8Service.listar();
    }

    @GetMapping("/{pessoa8Id}")
    public ResponseEntity<Pessoa8> buscar(@PathVariable Long pessoa8Id){
        Optional<Pessoa8> pessoa8 = this.pessoa8Service.getPessoa8(pessoa8Id);
        if(pessoa8.isPresent()){
            return ResponseEntity.ok(pessoa8.get());
        }
        return ResponseEntity.notFound().build();
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public Pessoa8 adicionar(@Valid @RequestBody Pessoa8 pessoa8){
        return this.pessoa8Service.salvar(pessoa8);
    }

    @PutMapping("{pessoa8Id}")
    public ResponseEntity<Pessoa8> atualizar(@PathVariable Long pessoa8Id,@Valid @RequestBody Pessoa8 pessoa8){
        if(!this.pessoa8Service.existeId(pessoa8Id)){
            return ResponseEntity.notFound().build();
        }
        pessoa8.setId(pessoa8Id);
        pessoa8 = this.pessoa8Service.salvar(pessoa8);
        return ResponseEntity.ok(pessoa8);
    }

    @DeleteMapping("/{pessoa8Id}")
    public ResponseEntity<Void> excluir(@PathVariable Long pessoa8Id){
        if(!this.pessoa8Service.existeId(pessoa8Id)){
            return ResponseEntity.notFound().build();
        }
        this.pessoa8Service.excluir(pessoa8Id);
        return ResponseEntity.noContent().build();
    }
}
