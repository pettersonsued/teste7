package com.example.petter.api.controller;

import com.example.petter.domain.model.Endereco;
import com.example.petter.domain.service.EnderecoService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@RestController
@RequestMapping("enderecos")
public class EnderecoController {

    private EnderecoService enderecoService;

    @Operation(summary = "realiza a listagem de todas os Endereços", method = "POST")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Endreço encontrado com sucesso"),
            @ApiResponse(responseCode = "422", description = "Dados de requisição inválida"),
            @ApiResponse(responseCode = "400", description = "Parametros inválidos"),
            @ApiResponse(responseCode = "404", description = "Parametros inválidos"),
            @ApiResponse(responseCode = "500", description = "Erro no servidor"),
    })
    @GetMapping
    public List<Endereco> listar(){
        return this.enderecoService.listar();
    }

    @Operation(summary = "realiza a busca de um Endereço por Id", method = "POST")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Endereço encontrado"),
            @ApiResponse(responseCode = "422", description = "Dados de requisição inválida"),
            @ApiResponse(responseCode = "400", description = "Parametros inválidos"),
            @ApiResponse(responseCode = "404", description = "Parametros inválidos"),
            @ApiResponse(responseCode = "500", description = "Erro no servidor"),
    })
    @GetMapping("/{enderecoId}")
    public ResponseEntity<Endereco> buscar(@PathVariable Long enderecoId){
        Optional<Endereco> endereco = this.enderecoService.getEndereco(enderecoId);
        if(endereco.isPresent()){
            return ResponseEntity.ok(endereco.get());
        }
        return ResponseEntity.notFound().build();
    }

    @Operation(summary = "realiza o cadastro de uma Endereço", method = "POST")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Endereço Cadastrado com sucesso"),
            @ApiResponse(responseCode = "422", description = "Dados de requisição inválida"),
            @ApiResponse(responseCode = "400", description = "Parametros inválidos"),
            @ApiResponse(responseCode = "404", description = "Parametros inválidos"),
            @ApiResponse(responseCode = "500", description = "Erro no servidor"),
    })
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public Endereco adicionar(@RequestBody Endereco endereco){
        return this.enderecoService.salvar(endereco);
    }

    @Operation(summary = "realiza a atualização de uma Endereço", method = "POST")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Endereço encontrado"),
            @ApiResponse(responseCode = "422", description = "Dados de requisição inválida"),
            @ApiResponse(responseCode = "400", description = "Parametros inválidos"),
            @ApiResponse(responseCode = "404", description = "Parametros inválidos"),
            @ApiResponse(responseCode = "500", description = "Erro no servidor"),
    })
    @PutMapping("/{enderecoId}")
    public ResponseEntity<Endereco> atualizar(@PathVariable Long enderecoId, @RequestBody Endereco endereco){
        if(!this.enderecoService.existeId(enderecoId)){
            return ResponseEntity.notFound().build();
        }
        endereco.setId(enderecoId);
        endereco = this.enderecoService.salvar(endereco);
        return ResponseEntity.ok(endereco);
    }

    @Operation(summary = "realiza a exclusão de um Endereço", method = "POST")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Endereço encontrado"),
            @ApiResponse(responseCode = "422", description = "Dados de requisição inválida"),
            @ApiResponse(responseCode = "400", description = "Parametros inválidos"),
            @ApiResponse(responseCode = "404", description = "Parametros inválidos"),
            @ApiResponse(responseCode = "500", description = "Erro no servidor"),
    })
    @DeleteMapping("/{enderecoId}")
    public ResponseEntity<Void> excluir(@PathVariable Long enderecoId){
        if(!this.enderecoService.existeId(enderecoId)){
            return ResponseEntity.notFound().build();
        }
        this.enderecoService.excluir(enderecoId);
        return ResponseEntity.noContent().build();
    }
}
