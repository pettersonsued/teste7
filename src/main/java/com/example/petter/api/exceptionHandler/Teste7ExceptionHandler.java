package com.example.petter.api.exceptionHandler;

import com.example.petter.domain.exception.CadastroException;
import com.example.petter.domain.exception.EntidadeNaoEncontradaException;
import lombok.AllArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@ControllerAdvice
public class Teste7ExceptionHandler extends ResponseEntityExceptionHandler {

    private MessageSource ms;


    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers, HttpStatus status, WebRequest request){
        List<Problema.Campo> campos = new ArrayList<>();

        for(ObjectError erro : ex.getBindingResult().getAllErrors()) {
            String nome = ((FieldError) erro).getField();
            String msg = ms.getMessage(erro, LocaleContextHolder.getLocale());

            campos.add(new Problema.Campo(nome, msg));
        }
        Problema problema = new Problema();
        problema.setStatus(status.value());
        problema.setDataHora(OffsetDateTime.now());
        problema.setTitulo("Campo Inváidos, Preencha Corretamente");
        problema.setCampos(campos);

        return super.handleExceptionInternal(ex, problema, headers, status, request);
    }


    @ExceptionHandler(EntidadeNaoEncontradaException.class)
    public ResponseEntity<Object> handleEntidadeNaoEncontradaException(EntidadeNaoEncontradaException ex, WebRequest request){
        HttpStatus status = HttpStatus.NOT_FOUND;
        Problema problema = new Problema();
        problema.setStatus(status.value());
        problema.setDataHora(OffsetDateTime.now());
        problema.setTitulo(ex.getMessage());

        return handleExceptionInternal(ex, problema, new HttpHeaders(), status, request);
    }

    @ExceptionHandler(CadastroException.class)
    public ResponseEntity<Object> handleCadastroException(CadastroException ce, WebRequest request){
        HttpStatus status = HttpStatus.NOT_FOUND;
        Problema problema = new Problema();
        problema.setStatus(status.value());
        problema.setDataHora(OffsetDateTime.now());
        problema.setTitulo(ce.getMessage());
        return handleExceptionInternal(ce, problema, new HttpHeaders(), status, request);
    }
}
