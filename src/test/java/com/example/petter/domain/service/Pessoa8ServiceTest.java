package com.example.petter.domain.service;

import com.example.petter.domain.model.Pessoa8;
import com.example.petter.domain.repository.Pessoa8Repository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.mockito.Mockito.*;

@SpringBootTest
@AutoConfigureMockMvc
public class Pessoa8ServiceTest {

    @Autowired
    private Pessoa8Service pessoa8Service;

    @MockBean
    private Pessoa8Repository pessoa8Repository;



    @Test
    public void deveretornarumaListadePessoa8(){
        List<Pessoa8> list =this.getPessoa8s();

        when(this.pessoa8Repository.findAll()).thenReturn(list);

        List<Pessoa8> list2 = this.pessoa8Service.listar();

        assertNotNull(list2);
        verify(this.pessoa8Repository,times(1)).findAll();
    }

    @Test
    public void  deveadicionarumaPessoa8(){
        Pessoa8 p = new Pessoa8();
        p.setId(1L);
        p.setNome("Osmair");
        p.setEmail("osmair@hotmail.com.br");
        p.setFone("1213333-1111");
        p.setDataNascimento(new Date());

        when(this.pessoa8Repository.save(p)).thenReturn(p);

        Pessoa8 pessoa8 = this.pessoa8Service.salvar(p);

        assertNotNull(pessoa8);
        verify(this.pessoa8Repository,times(1)).save(p);
    }

    @Test
    public void deveretornarumaPessoa8podId(){
        Long pessoa8Id=1L;

        Pessoa8 p = new Pessoa8();
        p.setId(1L);
        p.setNome("Osmair");
        p.setEmail("osmair@hotmail.com.br");
        p.setFone("1213333-1111");
        p.setDataNascimento(new Date());


        when(this.pessoa8Repository.findById(pessoa8Id)).thenReturn(Optional.of(p));

        Optional<Pessoa8> pessoa8 = this.pessoa8Service.getPessoa8(pessoa8Id);

        assertNotNull(pessoa8);
        verify(this.pessoa8Repository,times(1)).findById(pessoa8Id);
    }


    @Test
    public void deveretornarTrue(){
        Long pessoa8Id=1L;

        when(this.pessoa8Repository.existsById(pessoa8Id)).thenReturn(true);

        boolean resposta = this.pessoa8Service.existeId(pessoa8Id);

        assertNotNull(resposta);
        verify(this.pessoa8Repository,times(1)).existsById(pessoa8Id);
    }

    @Test
    public void deveexcluirumaPessoa8(){
        Long pessoa8Id=1L;

        doNothing().when(this.pessoa8Repository).deleteById(pessoa8Id);

        this.pessoa8Service.excluir(pessoa8Id);

        verify(this.pessoa8Repository,times(1)).deleteById(pessoa8Id);
    }




    public List<Pessoa8> getPessoa8s(){
        List<Pessoa8> list = new ArrayList<>();
        Pessoa8 p = new Pessoa8();
        p.setId(1L);
        p.setNome("Osmair");
        p.setEmail("osmair@hotmail.com.br");
        p.setFone("1213333-1111");
        p.setDataNascimento(new Date());
        list.add(p);

        Pessoa8 p2 = new Pessoa8();
        p2.setId(1L);
        p2.setNome("Osmair");
        p2.setEmail("osmair@hotmail.com.br");
        p2.setFone("1213333-1111");
        p2.setDataNascimento(new Date());
        list.add(p2);

        return list;
    }


}
