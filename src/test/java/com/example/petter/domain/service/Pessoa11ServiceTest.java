package com.example.petter.domain.service;

import com.example.petter.domain.model.Pessoa11;
import com.example.petter.domain.repository.Pessoa11Repository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.mockito.Mockito.*;

@SpringBootTest
@AutoConfigureMockMvc
public class Pessoa11ServiceTest {

    @Autowired
    private Pessoa11Service pessoa11Service;

    @MockBean
    private Pessoa11Repository pessoa11Repository;


    @Test
    public void deveretornarumaListadePesoa11(){
        List<Pessoa11> list = this.getPessoa11s();

        when(this.pessoa11Repository.findAll()).thenReturn(list);

        List<Pessoa11> list2 = this.pessoa11Service.listar();

        assertNotNull(list2);
        verify(this.pessoa11Repository, times(1)).findAll();
    }

    @Test
    public void deveadicionarumaPessoa11(){
        Pessoa11 p = new Pessoa11();
        p.setId(1L);
        p.setNome("Sueder");
        p.setEmail("sueder@hotmail.com.br");
        p.setFone("1213333-1111");
        p.setDataNascimento(new Date());

        when(this.pessoa11Repository.save(p)).thenReturn(p);

        Pessoa11 pessoa11 = this.pessoa11Service.salvar(p);

        assertNotNull(pessoa11);
        verify(this.pessoa11Repository,times(1)).save(p);
    }

    @Test
    public void deveretornarumaPessoa11porId(){
        Long pessoa11Id=1L;

        Pessoa11 p = new Pessoa11();
        p.setId(1L);
        p.setNome("Sueder");
        p.setEmail("sueder@hotmail.com.br");
        p.setFone("1213333-1111");
        p.setDataNascimento(new Date());

        when(this.pessoa11Repository.findById(pessoa11Id)).thenReturn(Optional.of(p));

        Optional<Pessoa11> pessoa11 = this.pessoa11Service.getPessoa11(pessoa11Id);

        assertNotNull(pessoa11);
        verify(this.pessoa11Repository,times(1)).findById(pessoa11Id);
    }


    @Test
    public void deveretornarTrue(){
        Long pessoa11Id=1L;

        when(this.pessoa11Repository.existsById(pessoa11Id)).thenReturn(true);

        boolean resposta = this.pessoa11Service.existeId(pessoa11Id);

        assertNotNull(resposta);
        verify(this.pessoa11Repository,times(1)).existsById(pessoa11Id);
    }



    @Test
    public void deveexcluirUmaPessoa11(){
        Long pessoa11Id=1L;

        doNothing().when(this.pessoa11Repository).deleteById(pessoa11Id);

        this.pessoa11Service.excluir(pessoa11Id);

        verify(this.pessoa11Repository,times(1)).deleteById(pessoa11Id);
    }








    public List<Pessoa11> getPessoa11s(){
        List<Pessoa11> list = new ArrayList<>();
        Pessoa11 p = new Pessoa11();
        p.setId(1L);
        p.setNome("Sueder");
        p.setEmail("sueder@hotmail.com.br");
        p.setFone("1213333-1111");
        p.setDataNascimento(new Date());
        list.add(p);

        Pessoa11 p2 = new Pessoa11();
        p2.setId(1L);
        p2.setNome("Sueder");
        p2.setEmail("sueder@hotmail.com.br");
        p2.setFone("1213333-1111");
        p2.setDataNascimento(new Date());
        list.add(p2);

        return list;
    }
}
