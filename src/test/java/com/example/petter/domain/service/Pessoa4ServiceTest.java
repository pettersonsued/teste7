package com.example.petter.domain.service;

import com.example.petter.domain.model.Pessoa4;
import com.example.petter.domain.repository.Pessoa4Repository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.mockito.Mockito.*;

@SpringBootTest
@AutoConfigureMockMvc
public class Pessoa4ServiceTest {


    @Autowired
    private Pessoa4Service pessoa4Service;

    @MockBean
    private Pessoa4Repository pessoa4Repository;


    @Test
    public void deveretornarumalistadePessoa4(){
        List<Pessoa4> list = this.getPessoa4s();

        when(this.pessoa4Repository.findAll()).thenReturn(list);

        List<Pessoa4> list2 = this.pessoa4Service.listar();

        assertNotNull(list2);
        verify(this.pessoa4Repository,times(1)).findAll();
    }

    @Test
    public void deveadicionarumaPessoa4(){
        Pessoa4 p = new Pessoa4();
        p.setId(1L);
        p.setNome("Marciano");
        p.setEmail("marciano@hotmail.com.br");
        p.setFone("1223333-1111");
        p.setDataNascimento(new Date());

        when(this.pessoa4Repository.save(p)).thenReturn(p);


        Pessoa4 pessoa4 = this.pessoa4Service.salvar(p);

        assertNotNull(pessoa4);
        verify(this.pessoa4Repository,times(1)).save(p);
    }

    @Test
    public void deveretornarumaPessoaporId(){
        Long pessoa4Id=1L;

        Pessoa4 p = new Pessoa4();
        p.setId(1L);
        p.setNome("Marciano");
        p.setEmail("marciano@hotmail.com.br");
        p.setFone("1223333-1111");
        p.setDataNascimento(new Date());

        when(this.pessoa4Repository.findById(pessoa4Id)).thenReturn(Optional.of(p));

        Optional<Pessoa4> pessoa4 = this.pessoa4Service.getPessoa4(pessoa4Id);

        assertNotNull(pessoa4);
        verify(this.pessoa4Repository,times(1)).findById(pessoa4Id);
    }

    @Test
    public void deveretornarTrue(){
        Long pessoa4Id=1L;

        when(this.pessoa4Repository.existsById(pessoa4Id)).thenReturn(true);

        boolean resposta =this.pessoa4Service.existeId(pessoa4Id);

        assertNotNull(resposta);
        verify(this.pessoa4Repository,times(1)).existsById(pessoa4Id);
    }


    @Test
    public void deveexcluirumaPessoa4(){
        Long pessoa4Id=1L;

        doNothing().when(this.pessoa4Repository).deleteById(pessoa4Id);

        this.pessoa4Service.excluir(pessoa4Id);
        verify(this.pessoa4Repository, times(1)).deleteById(pessoa4Id);
    }






    public List<Pessoa4> getPessoa4s(){
        List<Pessoa4> list = new ArrayList<>();
        Pessoa4 p = new Pessoa4();
        p.setId(1L);
        p.setNome("Marciano");
        p.setEmail("marciano@hotmail.com.br");
        p.setFone("1223333-1111");
        p.setDataNascimento(new Date());
        list.add(p);

        Pessoa4 p2 = new Pessoa4();
        p2.setId(1L);
        p2.setNome("Marciano");
        p2.setEmail("marciano@hotmail.com.br");
        p2.setFone("1223333-1111");
        p2.setDataNascimento(new Date());
        list.add(p2);

        return list;
    }
}
