package com.example.petter.domain.service;

import com.example.petter.domain.model.Endereco;
import com.example.petter.domain.model.Pessoa;
import com.example.petter.domain.repository.PessoaRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@SpringBootTest
@AutoConfigureMockMvc
public class PessoaServiceTest {


    @Autowired
    private PessoaService pessoaService;

    @MockBean
    private PessoaRepository pessoaRepository;



    @Test
    public void deveretornarumalistadePessoas(){

        List<Pessoa> list = this.getPessoas();

        when(this.pessoaRepository.findAll()).thenReturn(list);

        List<Pessoa> list2 = this.pessoaService.listar();

        assertNotNull(list2);
    }

    @Test
    public void devesalvarumPessoa(){
        Endereco e = new Endereco();
        e.setId(1L);
        e.setLogradouro("teste");
        e.setCep("88233333");

        Pessoa p = new Pessoa();
        p.setId(1L);
        p.setNome("João");
        p.setDataNascimento(new Date());
        p.setEndereco(e);


        when(pessoaRepository.save(p)).thenReturn(p);

        Pessoa pessoa = this.pessoaService.salvar(p);

        assertNotNull(pessoa);
    }

    @Test
    public void devebuscarumaPessoaPorid(){
        Endereco e = new Endereco();
        e.setId(1L);
        e.setLogradouro("teste");
        e.setCep("88233333");

        Pessoa p = new Pessoa();
        p.setId(1L);
        p.setNome("João");
        p.setDataNascimento(new Date());
        p.setEndereco(e);

        Long pessoaId=1L;

        when(this.pessoaRepository.findById(pessoaId)).thenReturn(Optional.of(p));

        Optional<Pessoa> optional = this.pessoaService.getPessoa(pessoaId);

        assertNotNull(optional);
    }

    @Test
    public void deveretornarTrue(){
        Long pessoaId=1L;

        when(this.pessoaRepository.existsById(pessoaId)).thenReturn(true);

        boolean resposta = this.pessoaService.existeId(pessoaId);

        assertNotNull(resposta);

    }

    @Test
    public void deveexcluirumaPessoa(){
        Long pessoaId=1L;

        doNothing().when(this.pessoaRepository).deleteById(pessoaId);

        this.pessoaService.excluir(pessoaId);
    }






    public List<Pessoa> getPessoas(){
        List<Pessoa> list = new ArrayList<>();
        Pessoa e = new Pessoa();
        e.setId(1L);
        e.setNome("João");
        e.setDataNascimento(new Date());
        list.add(e);

        Pessoa e2 = new Pessoa();
        e2.setId(1L);
        e2.setNome("João");
        e2.setDataNascimento(new Date());
        list.add(e2);

        return list;
    }
}
