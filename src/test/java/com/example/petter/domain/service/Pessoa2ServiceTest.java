package com.example.petter.domain.service;


import com.example.petter.domain.model.Pessoa2;
import com.example.petter.domain.repository.Pessoa2Repository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.mockito.Mockito.*;

@SpringBootTest
@AutoConfigureMockMvc
public class Pessoa2ServiceTest {

    @Autowired
    private Pessoa2Service pessoa2Service;

    @MockBean
    private Pessoa2Repository pessoa2Repository;


    @Test
    public void deveretornarumaListadePessoa2(){
        List<Pessoa2> list = this.getPessoa2s();

        when(this.pessoa2Repository.findAll()).thenReturn(list);

        List<Pessoa2> list2 = this.pessoa2Service.listar();

        assertNotNull(list2);
        verify(this.pessoa2Repository,times(1)).findAll();
    }

    @Test
    public void deveadicionarumaPessoa2(){
        Pessoa2 p = new Pessoa2();
        p.setId(1L);
        p.setNome("Amanda");
        p.setEmail("amanda@hotmail.com.br");
        p.setFone("2123333-2222");
        p.setDataNascimento(new Date());

        when(this.pessoa2Repository.save(p)).thenReturn(p);

        Pessoa2 pessoa2 = this.pessoa2Service.salvar(p);

        assertNotNull(pessoa2);
        verify(this.pessoa2Repository,times(1)).save(p);
    }

    @Test
    public void deveretornarumaPessoa2porId(){
        Long pessoa2Id=1L;

        Pessoa2 p = new Pessoa2();
        p.setId(1L);
        p.setNome("Amanda");
        p.setEmail("amanda@hotmail.com.br");
        p.setFone("2123333-2222");
        p.setDataNascimento(new Date());


        when(this.pessoa2Repository.findById(pessoa2Id)).thenReturn(Optional.of(p));

        Optional<Pessoa2> pessoa2 = this.pessoa2Service.getPessoa2(pessoa2Id);

        assertNotNull(pessoa2);
        verify(this.pessoa2Repository,times(1)).findById(pessoa2Id);
    }

    @Test
    public void deveretornarTrue(){
        Long pessoa2Id=1L;

        when(this.pessoa2Repository.existsById(pessoa2Id)).thenReturn(true);

        boolean resposta = this.pessoa2Service.existeId(pessoa2Id);

        assertNotNull(resposta);
        verify(this.pessoa2Repository,times(1)).existsById(pessoa2Id);
    }

    @Test
    public void deveexcluirumaPessoa2(){
        Long pessoa2Id=1L;

        doNothing().when(this.pessoa2Repository).deleteById(pessoa2Id);

        this.pessoa2Service.excluir(pessoa2Id);

        verify(this.pessoa2Repository,times(1)).deleteById(pessoa2Id);
    }





    public List<Pessoa2> getPessoa2s(){
        List<Pessoa2> list = new ArrayList<>();
        Pessoa2 p = new Pessoa2();
        p.setId(1L);
        p.setNome("Amanda");
        p.setEmail("amanda@hotmail.com.br");
        p.setFone("2123333-2222");
        p.setDataNascimento(new Date());
        list.add(p);

        Pessoa2 p2 = new Pessoa2();
        p2.setId(1L);
        p2.setNome("Amanda");
        p2.setEmail("amanda@hotmail.com.br");
        p2.setFone("2123333-2222");
        p2.setDataNascimento(new Date());
        list.add(p2);

        return list;
    }
}
