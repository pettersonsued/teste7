package com.example.petter.domain.service;

import com.example.petter.domain.model.Endereco;
import com.example.petter.domain.repository.EnderecoRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@SpringBootTest
@AutoConfigureMockMvc
public class EnderecoServiceTest {

    @Autowired
    private EnderecoService enderecoService;

    @MockBean
    private EnderecoRepository enderecoRepository;

    @Test
    public void deveretornarumalistadeEnderecos(){

        List<Endereco> list = this.getEnderecos();
        when(this.enderecoRepository.findAll()).thenReturn(list);

        List<Endereco> list2 = this.enderecoService.listar();

        assertNotNull(list2);
    }

    @Test
    public void devesalvarumEndereco(){
        Endereco e = new Endereco();
        e.setId(1L);
        e.setLogradouro("teste");
        e.setCep("88233333");


        when(enderecoRepository.save(e)).thenReturn(e);

        Endereco endereco = this.enderecoService.salvar(e);

        assertNotNull(endereco);
    }

    @Test
    public void devebuscarumenderecoPorid(){
        Endereco e = new Endereco();
        e.setId(1L);
        e.setLogradouro("teste");
        e.setCep("88233333");

        Long enderecoId=1L;

        when(this.enderecoRepository.findById(enderecoId)).thenReturn(Optional.of(e));

        Optional<Endereco> optional = this.enderecoService.getEndereco(enderecoId);

        assertNotNull(optional);
    }


    @Test
    public void deveretornarTrue(){
        Long produtoId=1L;

        when(this.enderecoRepository.existsById(produtoId)).thenReturn(true);

        boolean resposta = this.enderecoService.existeId(produtoId);

        assertNotNull(resposta);

    }

    @Test
    public void deveexcluirumProduto(){
        Long produtoId=1L;

        doNothing().when(this.enderecoRepository).deleteById(produtoId);

        this.enderecoService.excluir(produtoId);
    }


    public List<Endereco> getEnderecos(){
        List<Endereco> list = new ArrayList<>();
        Endereco e = new Endereco();
        e.setId(1L);
        e.setLogradouro("teste");
        e.setCep("88233333");
        list.add(e);


        Endereco e2 = new Endereco();
        e2.setId(1L);
        e2.setLogradouro("teste");
        e2.setCep("88233333");
        list.add(e2);

        return list;
    }
}
