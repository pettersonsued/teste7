package com.example.petter.domain.service;

import com.example.petter.domain.model.Pessoa6;
import com.example.petter.domain.repository.Pessoa6Repository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.mockito.Mockito.*;

@SpringBootTest
@AutoConfigureMockMvc
public class Pessoa6ServiceTest {


    @Autowired
    private Pessoa6Service pessoa6Service;


    @MockBean
    private Pessoa6Repository pessoa6Repository;



    @Test
    public void deveretornarumalistadePessoa6(){
        List<Pessoa6> list = this.getPessoa6s();

        when(this.pessoa6Repository.findAll()).thenReturn(list);

        List<Pessoa6> list2 = this.pessoa6Service.listar();

        assertNotNull(list2);
        verify(this.pessoa6Repository,times(1)).findAll();
    }

    @Test
    public void deveadicionarumaPessoa6(){
        Pessoa6 p = new Pessoa6();
        p.setId(1L);
        p.setNome("Amauri");
        p.setEmail("amauri@hotmail.com.br");
        p.setFone("1213333-1111");
        p.setDataNascimento(new Date());

        when(this.pessoa6Repository.save(p)).thenReturn(p);

        Pessoa6 pessoa6 = this.pessoa6Service.salvar(p);

        assertNotNull(pessoa6);
        verify(this.pessoa6Repository,times(1)).save(p);
    }

    @Test
    public void deveretornarumaPessoa6porId(){
        Long pessoa6Id1L=1L;

        Pessoa6 p = new Pessoa6();
        p.setId(1L);
        p.setNome("Amauri");
        p.setEmail("amauri@hotmail.com.br");
        p.setFone("1213333-1111");
        p.setDataNascimento(new Date());

        when(this.pessoa6Repository.findById(pessoa6Id1L)).thenReturn(Optional.of(p));

        Optional<Pessoa6> pessoa6 = this.pessoa6Service.getPessoa6(pessoa6Id1L);

        assertNotNull(pessoa6);
        verify(this.pessoa6Repository,times(1)).findById(pessoa6Id1L);
    }

    @Test
    public void deveretornarTrue(){
        Long pessoa6Id=1L;

        when(this.pessoa6Repository.existsById(pessoa6Id)).thenReturn(true);

        boolean resposta = this.pessoa6Service.existeId(pessoa6Id);

        assertNotNull(resposta);
        verify(this.pessoa6Repository,times(1)).existsById(pessoa6Id);
    }

    @Test
    public void deveexcluirumaPessoa6(){
        Long pessoa6Id=1L;

        doNothing().when(this.pessoa6Repository).deleteById(pessoa6Id);

        this.pessoa6Service.excluir(pessoa6Id);

        verify(this.pessoa6Repository,times(1)).deleteById(pessoa6Id);
    }


    public List<Pessoa6> getPessoa6s(){
        List<Pessoa6> list = new ArrayList<>();
        Pessoa6 p = new Pessoa6();
        p.setId(1L);
        p.setNome("Amauri");
        p.setEmail("amauri@hotmail.com.br");
        p.setFone("1213333-1111");
        p.setDataNascimento(new Date());
        list.add(p);

        Pessoa6 p2 = new Pessoa6();
        p2.setId(1L);
        p2.setNome("Amauri");
        p2.setEmail("amauri@hotmail.com.br");
        p2.setFone("1213333-1111");
        p2.setDataNascimento(new Date());
        list.add(p2);

        return list;
    }
}
