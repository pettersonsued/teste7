package com.example.petter.domain.service;

import com.example.petter.domain.model.Pessoa9;
import com.example.petter.domain.repository.Pessoa9Repository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.mockito.Mockito.*;

@SpringBootTest
@AutoConfigureMockMvc
public class Pessoa9ServiceTest {

    @Autowired
    private Pessoa9Service pessoa9Service;

    @MockBean
    private Pessoa9Repository pessoa9Repository;


    @Test
    public void deveretornarumaListadePessoa9(){
        List<Pessoa9> list =this.getPessoa9s();

        when(this.pessoa9Repository.findAll()).thenReturn(list);

        List<Pessoa9> list2 = this.pessoa9Service.listar();

        assertNotNull(list2);
        verify(this.pessoa9Repository,times(1)).findAll();
    }

    @Test
    public void deveadicionarumaPessoa9(){
        Pessoa9 p = new Pessoa9();
        p.setId(1L);
        p.setNome("Malafaia");
        p.setEmail("malafaia@hotmail.com.br");
        p.setFone("1213333-2222");
        p.setDataNascimento(new Date());

        when(this.pessoa9Repository.save(p)).thenReturn(p);

        Pessoa9 pessoa9 =this.pessoa9Service.salvar(p);

        assertNotNull(pessoa9);
        verify(this.pessoa9Repository,times(1)).save(p);
    }

    @Test
    public void deveretornarumaPessoa9porId(){
        Long pessoa9Id=1L;

        Pessoa9 p = new Pessoa9();
        p.setId(1L);
        p.setNome("Malafaia");
        p.setEmail("malafaia@hotmail.com.br");
        p.setFone("1213333-2222");
        p.setDataNascimento(new Date());

        when(this.pessoa9Repository.findById(pessoa9Id)).thenReturn(Optional.of(p));

        Optional<Pessoa9> pessoa9 =this.pessoa9Service.getPessoa9(pessoa9Id);

        assertNotNull(pessoa9);
        verify(this.pessoa9Repository,times(1)).findById(pessoa9Id);
    }

    @Test
    public void deveretornarTrue(){
        Long pessoa9Id=1L;

        when(this.pessoa9Repository.existsById(pessoa9Id)).thenReturn(true);

        boolean resposta = this.pessoa9Service.existeId(pessoa9Id);

        assertNotNull(resposta);
        verify(this.pessoa9Repository,times(1)).existsById(pessoa9Id);
    }

    @Test
    public void deveexcluirumaPessoa9(){
        Long pessoa9Id=1L;

        doNothing().when(this.pessoa9Repository).deleteById(pessoa9Id);

        this.pessoa9Service.excluir(pessoa9Id);

        verify(this.pessoa9Repository,times(1)).deleteById(pessoa9Id);
    }




    public List<Pessoa9> getPessoa9s(){
        List<Pessoa9> list = new ArrayList<>();
        Pessoa9 p = new Pessoa9();
        p.setId(1L);
        p.setNome("Malafaia");
        p.setEmail("malafaia@hotmail.com.br");
        p.setFone("1213333-2222");
        p.setDataNascimento(new Date());
        list.add(p);

        Pessoa9 p2 = new Pessoa9();
        p2.setId(1L);
        p2.setNome("Malafaia");
        p2.setEmail("malafaia@hotmail.com.br");
        p2.setFone("1213333-2222");
        p2.setDataNascimento(new Date());
        list.add(p2);

        return list;
    }
}
