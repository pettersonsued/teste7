package com.example.petter.domain.service;

import com.example.petter.domain.model.Pessoa10;
import com.example.petter.domain.repository.Pessoa10Repository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.mockito.Mockito.*;

@SpringBootTest
@AutoConfigureMockMvc
public class Pessoa10ServiceTest {

    @Autowired
    private Pessoa10Service pessoa10Service;

    @MockBean
    private Pessoa10Repository pessoa10Repository;


    @Test
    public void deveretornarumaListadePessoa10(){
        List<Pessoa10> list =this.getPessoa10s();

        when(this.pessoa10Repository.findAll()).thenReturn(list);

        List<Pessoa10> list2 = this.pessoa10Service.listar();

        assertNotNull(list2);
        verify(this.pessoa10Repository,times(1)).findAll();
    }


    @Test
    public void deveadicionarumaPessoa10(){
        Pessoa10 p = new Pessoa10();
        p.setId(1L);
        p.setNome("Germam");
        p.setEmail("germam@hotmail.com.br");
        p.setFone("2123333-2222");
        p.setDataNascimento(new Date());

        when(this.pessoa10Repository.save(p)).thenReturn(p);

        Pessoa10 pessoa10 = this.pessoa10Service.salvar(p);

        assertNotNull(pessoa10);
        verify(this.pessoa10Repository,times(1)).save(p);
    }


    @Test
    public void deveretornarumaPessoaporId(){
        Long pessoa10Id=1L;

        Pessoa10 p = new Pessoa10();
        p.setId(1L);
        p.setNome("Germam");
        p.setEmail("germam@hotmail.com.br");
        p.setFone("2123333-2222");
        p.setDataNascimento(new Date());

        when(this.pessoa10Repository.findById(pessoa10Id)).thenReturn(Optional.of(p));

        Optional<Pessoa10> pessoa10 = this.pessoa10Service.getPessoa10(pessoa10Id);

        assertNotNull(pessoa10);
        verify(this.pessoa10Repository,times(1)).findById(pessoa10Id);
    }


    @Test
    public void deveretornarTrue(){
        Long pessoa10Id=1L;

        when(this.pessoa10Repository.existsById(pessoa10Id)).thenReturn(true);

        boolean resposta = this.pessoa10Service.existeId(pessoa10Id);

        assertNotNull(resposta);
        verify(this.pessoa10Repository,times(1)).existsById(pessoa10Id);
    }


    @Test
    public void deveexcluirumaPessoa10(){
        Long pessoa10Id=1L;

        doNothing().when(this.pessoa10Repository).deleteById(pessoa10Id);

        this.pessoa10Service.excluir(pessoa10Id);

        verify(this.pessoa10Repository,times(1)).deleteById(pessoa10Id);
    }





    public List<Pessoa10> getPessoa10s(){
        List<Pessoa10> list = new ArrayList<>();
        Pessoa10 p = new Pessoa10();
        p.setId(1L);
        p.setNome("Germam");
        p.setEmail("germam@hotmail.com.br");
        p.setFone("2123333-2222");
        p.setDataNascimento(new Date());
        list.add(p);

        Pessoa10 p2 = new Pessoa10();
        p2.setId(1L);
        p2.setNome("Germam");
        p2.setEmail("germam@hotmail.com.br");
        p2.setFone("2123333-2222");
        p2.setDataNascimento(new Date());
        list.add(p2);
        return list;
    }



}
