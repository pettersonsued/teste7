package com.example.petter.domain.service;

import com.example.petter.domain.model.Pessoa3;
import com.example.petter.domain.repository.Pessoa3Repository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.mockito.Mockito.*;

@SpringBootTest
@AutoConfigureMockMvc
public class Pessoa3ServiceTest {


    @Autowired
    private Pessoa3Service pessoa3Service;


    @MockBean
    private Pessoa3Repository pessoa3Repository;



    @Test
    public void deveretornarumaListadePessoa3(){
        List<Pessoa3> list = this.getPessoa3s();

        when(this.pessoa3Repository.findAll()).thenReturn(list);

        List<Pessoa3> list2 = this.pessoa3Service.listar();

        assertNotNull(list2);
        verify(this.pessoa3Repository,times(1)).findAll();
    }

    @Test
    public void deveadicionarumaPessoa3(){
        Pessoa3 p = new Pessoa3();
        p.setId(1L);
        p.setNome("josemar");
        p.setEmail("josemar@hotmail.com.br");
        p.setFone("1213333-2222");
        p.setDataNascimento(new Date());


        when(this.pessoa3Repository.save(p)).thenReturn(p);

        Pessoa3 pessoa3 = this.pessoa3Service.salvar(p);

        assertNotNull(pessoa3);
        verify(this.pessoa3Repository,times(1)).save(p);
    }

    @Test
    public void devertetornarumaPessoa3porId(){
        Pessoa3 p = new Pessoa3();
        p.setId(1L);
        p.setNome("josemar");
        p.setEmail("josemar@hotmail.com.br");
        p.setFone("1213333-2222");
        p.setDataNascimento(new Date());

        Long pessoa3Id =1l;

        when(this.pessoa3Repository.findById(pessoa3Id)).thenReturn(Optional.of(p));

        Optional<Pessoa3> pessoa3 = this.pessoa3Service.getPessoa3(pessoa3Id);

        assertNotNull(pessoa3);
        verify(this.pessoa3Repository,times(1)).findById(pessoa3Id);

    }

    @Test
    public void deveretornarTrue(){
        Long pessoa3Id=1L;

        when(this.pessoa3Repository.existsById(pessoa3Id)).thenReturn(true);

        boolean resposta = this.pessoa3Service.existeId(pessoa3Id);

        assertNotNull(resposta);
        verify(this.pessoa3Repository, times(1)).existsById(pessoa3Id);
    }

    @Test
    public void deveexcluirumaPessoa3(){
        Long pessoa3Id=1L;

        doNothing().when(this.pessoa3Repository).deleteById(pessoa3Id);

        this.pessoa3Service.excluir(pessoa3Id);
        verify(this.pessoa3Repository,times(1)).deleteById(pessoa3Id);
    }








    public List<Pessoa3> getPessoa3s(){
        List<Pessoa3> list = new ArrayList<>();
        Pessoa3 p = new Pessoa3();
        p.setId(1L);
        p.setNome("josemar");
        p.setEmail("josemar@hotmail.com.br");
        p.setFone("1213333-2222");
        p.setDataNascimento(new Date());
        list.add(p);

        Pessoa3 p2 = new Pessoa3();
        p2.setId(1L);
        p2.setNome("josemar");
        p2.setEmail("josemar@hotmail.com.br");
        p2.setFone("1213333-2222");
        p2.setDataNascimento(new Date());
        list.add(p2);

        return list;
    }
}
