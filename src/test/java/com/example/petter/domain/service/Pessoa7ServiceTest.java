package com.example.petter.domain.service;

import com.example.petter.domain.model.Pessoa7;
import com.example.petter.domain.repository.Pessoa7Repository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.mockito.Mockito.*;

@SpringBootTest
@AutoConfigureMockMvc
public class Pessoa7ServiceTest {

    @Autowired
    private Pessoa7Service pessoa7Service;


    @MockBean
    private Pessoa7Repository pessoa7Repository;




    @Test
    public void deveretornarumalistadePessoa7(){
        List<Pessoa7> list =this.getPessoa7s();

        when(this.pessoa7Repository.findAll()).thenReturn(list);

        List<Pessoa7> list2 = this.pessoa7Service.listar();

        assertNotNull(list2);
        verify(this.pessoa7Repository,times(1)).findAll();

    }

    @Test
    public void deveadicionarumapessoa7(){
        Pessoa7 p = new Pessoa7();
        p.setId(1L);
        p.setNome("Marli");
        p.setEmail("marli@hotmail.com.br");
        p.setFone("1213333-1111");
        p.setDataNascimento(new Date());

        when(this.pessoa7Repository.save(p)).thenReturn(p);

        Pessoa7 pessoa7 =this.pessoa7Service.salvar(p);

        assertNotNull(pessoa7);
        verify(this.pessoa7Repository,times(1)).save(p);
    }

    @Test
    public void deveretornarumaPessoa7porId(){
        Long pessoa7Id=1L;

        Pessoa7 p = new Pessoa7();
        p.setId(1L);
        p.setNome("Marli");
        p.setEmail("marli@hotmail.com.br");
        p.setFone("1213333-1111");
        p.setDataNascimento(new Date());

        when(this.pessoa7Repository.findById(pessoa7Id)).thenReturn(Optional.of(p));

        Optional<Pessoa7> pessoa7 = this.pessoa7Service.getPessoa7(pessoa7Id);

        assertNotNull(pessoa7);
        verify(this.pessoa7Repository,times(1)).findById(pessoa7Id);
    }

    @Test
    public void deveretornarTrue(){
        Long pessoa7Id=1L;

        when(this.pessoa7Repository.existsById(pessoa7Id)).thenReturn(true);

        boolean resposta = this.pessoa7Service.existeId(pessoa7Id);

        assertNotNull(resposta);
        verify(this.pessoa7Repository,times(1)).existsById(pessoa7Id);
    }

    @Test
    public void deveexcluirumaPessoa7(){
        Long pessoa7Id=1L;

        doNothing().when(this.pessoa7Repository).deleteById(pessoa7Id);

        this.pessoa7Service.excluir(pessoa7Id);

        verify(this.pessoa7Repository,times(1)).deleteById(pessoa7Id);
    }




    public List<Pessoa7> getPessoa7s(){
        List<Pessoa7> list = new ArrayList<>();
        Pessoa7 p = new Pessoa7();
        p.setId(1L);
        p.setNome("Marli");
        p.setEmail("marli@hotmail.com.br");
        p.setFone("1213333-1111");
        p.setDataNascimento(new Date());
        list.add(p);

        Pessoa7 p2 = new Pessoa7();
        p2.setId(1L);
        p2.setNome("Marli");
        p2.setEmail("marli@hotmail.com.br");
        p2.setFone("1213333-1111");
        p2.setDataNascimento(new Date());
        list.add(p2);

        return list;
    }
}
