package com.example.petter.domain.service;

import com.example.petter.domain.model.Pessoa5;
import com.example.petter.domain.repository.Pessoa5Repository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.mockito.Mockito.*;

@SpringBootTest
@AutoConfigureMockMvc
public class Pessoa5ServiceTest {

    @Autowired
    private Pessoa5Service pessoa5Service;

    @MockBean
    private Pessoa5Repository pessoa5Repository;


    @Test
    public void deveretornarumalistadePessoa5(){
        List<Pessoa5> list = this.getPessoa5s();

        when(this.pessoa5Repository.findAll()).thenReturn(list);

        List<Pessoa5> list2 = this.pessoa5Service.listar();

        assertNotNull(list2);
        verify(this.pessoa5Repository,times(1)).findAll();
    }

    @Test
    public void deveadicionarumaPessoa5(){
        Pessoa5 p = new Pessoa5();
        p.setId(1L);
        p.setNome("joacir");
        p.setEmail("joacir@hotmail.com.br");
        p.setFone("2124444-1111");
        p.setDataNascimento(new Date());

        when(this.pessoa5Repository.save(p)).thenReturn(p);

        Pessoa5 pessoa5 =this.pessoa5Service.salvar(p);

        assertNotNull(pessoa5);
        verify(this.pessoa5Repository,times(1)).save(p);
    }

    @Test
    public void deveretornarumaPessoa5porId(){
        Long pessoa5Id=1L;

        Pessoa5 p = new Pessoa5();
        p.setId(1L);
        p.setNome("joacir");
        p.setEmail("joacir@hotmail.com.br");
        p.setFone("2124444-1111");
        p.setDataNascimento(new Date());

        when(this.pessoa5Repository.findById(pessoa5Id)).thenReturn(Optional.of(p));

        Optional<Pessoa5> pessoa5 = this.pessoa5Service.getPessoa5(pessoa5Id);

        assertNotNull(pessoa5);
        verify(this.pessoa5Repository,times(1)).findById(pessoa5Id);
    }

    @Test
    public void deveretornarTrue(){
        Long pessoa5Id=1L;

        when(this.pessoa5Repository.existsById(pessoa5Id)).thenReturn(true);

        boolean resposta = this.pessoa5Service.existeId(pessoa5Id);

        assertNotNull(resposta);
        verify(this.pessoa5Repository,times(1)).existsById(pessoa5Id);
    }

    @Test
    public void deveexcluirumaPessoa5(){
        Long pessoa5Id=1L;

        doNothing().when(this.pessoa5Repository).deleteById(pessoa5Id);

        this.pessoa5Service.excluir(pessoa5Id);
        verify(this.pessoa5Repository,times(1)).deleteById(pessoa5Id);
    }

    public List<Pessoa5> getPessoa5s(){
        List<Pessoa5> list = new ArrayList<>();
        Pessoa5 p = new Pessoa5();
        p.setId(1L);
        p.setNome("joacir");
        p.setEmail("joacir@hotmail.com.br");
        p.setFone("2124444-1111");
        p.setDataNascimento(new Date());
        list.add(p);

        Pessoa5 p2 = new Pessoa5();
        p2.setId(1L);
        p2.setNome("joacir");
        p2.setEmail("joacir@hotmail.com.br");
        p2.setFone("2124444-1111");
        p2.setDataNascimento(new Date());
        list.add(p2);

        return list;
    }





}
