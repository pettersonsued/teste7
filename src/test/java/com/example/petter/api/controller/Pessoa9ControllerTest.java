package com.example.petter.api.controller;


import com.example.petter.domain.model.Pessoa9;
import com.example.petter.domain.repository.Pessoa9Repository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Date;
import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@SpringBootTest
@AutoConfigureMockMvc
public class Pessoa9ControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private Pessoa9Repository pessoa9Repository;


    @Test
    public void deveretornarumaListadePessoa9() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/pessoa9s"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void deveretornarumaPessoaporId() throws Exception {
        Long pessoa9Id=1L;

        Pessoa9 p = new Pessoa9();
        p.setId(pessoa9Id);
        p.setNome("Joabe");
        p.setEmail("joabe@hotmail.com.br");
        p.setFone("9898888-1111");
        p.setDataNascimento(new Date());

        when(this.pessoa9Repository.findById(pessoa9Id)).thenReturn(Optional.of(p));

        mockMvc.perform(MockMvcRequestBuilders.get("/pessoa9s/{pessoa9Id}",pessoa9Id))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}
