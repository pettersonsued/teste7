package com.example.petter.api.controller;

import com.example.petter.domain.model.Pessoa4;
import com.example.petter.domain.repository.Pessoa4Repository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Date;
import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@SpringBootTest
@AutoConfigureMockMvc
public class Pessoa4ControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private Pessoa4Repository pessoa4Repository;



    @Test
    public void deveretornarumaListadePessoa4() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/pessoa4s"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void deveretornarumaPessoa4porId() throws Exception {
        Long pessoa4Id=1L;

        Pessoa4 p = new Pessoa4();
        p.setId(pessoa4Id);
        p.setNome("Mustafa");
        p.setEmail("mustafa@hotmail.com.br");
        p.setFone("1213333-1111");
        p.setDataNascimento(new Date());

        when(this.pessoa4Repository.findById(pessoa4Id)).thenReturn(Optional.of(p));

        mockMvc.perform(MockMvcRequestBuilders.get("/pessoa4s/{pessoa4Id}", pessoa4Id))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk());

    }
}
