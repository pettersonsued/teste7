package com.example.petter.api.controller;

import com.example.petter.domain.model.Pessoa7;
import com.example.petter.domain.repository.Pessoa7Repository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Date;
import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@SpringBootTest
@AutoConfigureMockMvc
public class Pessoa7ControllerTest {


    @Autowired
    private MockMvc mockMvc;


    @MockBean
    private Pessoa7Repository pessoa7Repository;



    @Test
    public void deveretornarumalistadePessoa7() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/pessoa7s"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void deveretornarumaPessoa7porId() throws Exception {
        Long pessoa7Id=1L;

        Pessoa7 p = new Pessoa7();
        p.setId(pessoa7Id);
        p.setNome("Joana");
        p.setEmail("joana@hotmail.com.br");
        p.setFone("1213333-2222");
        p.setDataNascimento(new Date());

        when(this.pessoa7Repository.findById(pessoa7Id)).thenReturn(Optional.of(p));

        mockMvc.perform(MockMvcRequestBuilders.get("/pessoa7s/{pessoa7Id}", pessoa7Id))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}
