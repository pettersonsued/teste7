package com.example.petter.api.controller;

import com.example.petter.domain.model.Pessoa10;
import com.example.petter.domain.repository.Pessoa10Repository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Date;
import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@SpringBootTest
@AutoConfigureMockMvc
public class Pessoa10ControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private Pessoa10Repository pessoa10Repository;


    @Test
    public void deveretornarumaListaPessoa10() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/pessoa10s"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void deveretornarumaPessoa10porId() throws Exception {
        Long pessoa10Id=1L;

        Pessoa10 p = new Pessoa10();
        p.setId(pessoa10Id);
        p.setNome("Josemar");
        p.setEmail("josemar@hotmail.com.br");
        p.setFone("1223333-1111");
        p.setDataNascimento(new Date());

        when(this.pessoa10Repository.findById(pessoa10Id)).thenReturn(Optional.of(p));

        mockMvc.perform(MockMvcRequestBuilders.get("/pessoa10s/{pessoa10Id}",pessoa10Id))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}
