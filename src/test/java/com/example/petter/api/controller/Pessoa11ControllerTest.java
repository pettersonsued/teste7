package com.example.petter.api.controller;

import com.example.petter.domain.model.Pessoa11;
import com.example.petter.domain.repository.Pessoa11Repository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Date;
import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@SpringBootTest
@AutoConfigureMockMvc
public class Pessoa11ControllerTest {


    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private Pessoa11Repository pessoa11Repository;



    @Test
    public void deveretornarumaListadePessoa11() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/pessoa11s"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void deveretornarumaPessoaporId() throws Exception {
        Long pesoa11Id=1L;

        Pessoa11 p = new Pessoa11();
        p.setId(pesoa11Id);
        p.setNome("Juliana");
        p.setEmail("juliana@hotmail.com.br");
        p.setFone("1213333-2222");
        p.setDataNascimento(new Date());

        when(this.pessoa11Repository.findById(pesoa11Id)).thenReturn(Optional.of(p));

        mockMvc.perform(MockMvcRequestBuilders.get("/pessoa11s/{pessoa11Id}", pesoa11Id))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}
