package com.example.petter.api.controller;

import com.example.petter.domain.model.Pessoa5;
import com.example.petter.domain.repository.Pessoa5Repository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Date;
import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@SpringBootTest
@AutoConfigureMockMvc
public class Pessoa5ControllerTest {



    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private Pessoa5Repository pessoa5Repository;



    @Test
    public void deveretornarUmalistadePessoa5() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/pessoa5s"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk());

    }

    @Test
    public void deveretornarumaPessoaporId() throws Exception {
        Long pessoa5Id=1L;

        Pessoa5 p =new Pessoa5();
        p.setId(pessoa5Id);
        p.setNome("Moacir");
        p.setEmail("moacir@hotmail.com.br");
        p.setFone("2123333-1111");
        p.setDataNascimento(new Date());

        when(this.pessoa5Repository.findById(pessoa5Id)).thenReturn(Optional.of(p));

        mockMvc.perform(MockMvcRequestBuilders.get("/pessoa5s/{pessoa5Id}",pessoa5Id))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}
