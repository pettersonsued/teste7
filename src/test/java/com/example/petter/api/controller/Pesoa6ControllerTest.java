package com.example.petter.api.controller;

import com.example.petter.domain.model.Pessoa6;
import com.example.petter.domain.repository.Pessoa6Repository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Date;
import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@SpringBootTest
@AutoConfigureMockMvc
public class Pesoa6ControllerTest {

    @Autowired
    private MockMvc mockMvc;


    @MockBean
    private Pessoa6Repository pessoa6Repository;


    @Test
    public void deveretornarumaListadePessoa6() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/pessoa6s"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void develistarumaPessoa6proId() throws Exception {
        Long pessoa6Id=1L;

        Pessoa6 p = new Pessoa6();
        p.setId(pessoa6Id);
        p.setNome("Maria");
        p.setEmail("maria@hotmail.com.br");
        p.setFone("1213333-1111");
        p.setDataNascimento(new Date());

        when(this.pessoa6Repository.findById(pessoa6Id)).thenReturn(Optional.of(p));

        mockMvc.perform(MockMvcRequestBuilders.get("/pessoa6s/{pessoa6Id}", pessoa6Id))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}
