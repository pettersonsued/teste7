package com.example.petter.api.controller;

import com.example.petter.domain.model.Endereco;
import com.example.petter.domain.model.Pessoa;
import com.example.petter.domain.repository.PessoaRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Date;
import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@SpringBootTest
@AutoConfigureMockMvc
public class PessoaControllerTest {


    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PessoaRepository pessoaRepository;

    @Test
    public void deveretornarumaListadePessoas() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/pessoas"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void deveretornarUmenderecoPorId() throws Exception {
        Long pessoaId = 1L;

        Pessoa e = new Pessoa();
        e.setId(pessoaId);
        e.setNome("João");
        e.setDataNascimento(new Date());


        when(this.pessoaRepository.findById(pessoaId)).thenReturn(Optional.of(e));

        mockMvc.perform(MockMvcRequestBuilders.get("/pessoas/{pessoaId}", pessoaId))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}
