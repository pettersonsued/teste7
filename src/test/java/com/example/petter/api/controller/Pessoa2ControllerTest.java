package com.example.petter.api.controller;

import com.example.petter.domain.model.Pessoa2;
import com.example.petter.domain.repository.Pessoa2Repository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Date;
import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@SpringBootTest
@AutoConfigureMockMvc
public class Pessoa2ControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private Pessoa2Repository pessoa2Repository;

    @Test
    public void deveretornarUmaListadePessoas2() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/pessoa2s"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void deveretornarumaPessoa2porId() throws Exception {
        Long pessoa2Id=1L;
        Pessoa2 p = new Pessoa2();
        p.setId(pessoa2Id);
        p.setNome("Izabel");
        p.setEmail("izabel@hotmail.com.br");
        p.setFone("2123333-2222");
        p.setDataNascimento(new Date());

        when(this.pessoa2Repository.findById(pessoa2Id)).thenReturn(Optional.of(p));

        mockMvc.perform(MockMvcRequestBuilders.get("/pessoa2s/{pessoa2Id}",pessoa2Id))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}
