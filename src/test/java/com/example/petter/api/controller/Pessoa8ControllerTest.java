package com.example.petter.api.controller;

import com.example.petter.domain.model.Pessoa8;
import com.example.petter.domain.repository.Pessoa8Repository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Date;
import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@SpringBootTest
@AutoConfigureMockMvc
public class Pessoa8ControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private Pessoa8Repository pessoa8Repository;


    @Test
    public void devertornarumaListadePeoosoa8() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/pessoa8s"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void deveretornarumaPessoa8Porid() throws Exception {
        Long pessoa8Id=1L;

        Pessoa8 p = new Pessoa8();
        p.setId(pessoa8Id);
        p.setNome("Luana");
        p.setEmail("luana@hotmail.com.br");
        p.setFone("9890000-8888");
        p.setDataNascimento(new Date());

        when(this.pessoa8Repository.findById(pessoa8Id)).thenReturn(Optional.of(p));

        mockMvc.perform(MockMvcRequestBuilders.get("/pessoa8s/{pessoa8Id}", pessoa8Id))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}
