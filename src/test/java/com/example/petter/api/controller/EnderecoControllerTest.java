package com.example.petter.api.controller;

import com.example.petter.domain.model.Endereco;
import com.example.petter.domain.repository.EnderecoRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@SpringBootTest
@AutoConfigureMockMvc
public class EnderecoControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EnderecoRepository enderecoRepository;

    @Test
    public void deveretornarumaListadeEnderecos() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/enderecos"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void deveretornarUmenderecoPorId() throws Exception {
        Long enderecoId = 1L;

        Endereco e = new Endereco();
        e.setId(enderecoId);
        e.setLogradouro("teste");
        e.setCep("88233333");

        when(this.enderecoRepository.findById(enderecoId)).thenReturn(Optional.of(e));

        mockMvc.perform(MockMvcRequestBuilders.get("/enderecos/{enderecoId}", enderecoId))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}
