package com.example.petter.api.controller;

import com.example.petter.domain.model.Pessoa3;
import com.example.petter.domain.repository.Pessoa3Repository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Date;
import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@SpringBootTest
@AutoConfigureMockMvc
public class Pessoa3ControllerTest {


    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private Pessoa3Repository pessoa3Repository;


    @Test
    public void deveretornarumaListadePessoa3() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/pessoa3s"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void deveretornarumaPessoa3porId() throws Exception {
        Long pessoa3Id=1L;

        Pessoa3 p = new Pessoa3();
        p.setId(pessoa3Id);
        p.setNome("joao");
        p.setEmail("joao@hotmail.com.br");
        p.setFone("2123333-1111");
        p.setDataNascimento(new Date());

        when(this.pessoa3Repository.findById(pessoa3Id)).thenReturn(Optional.of(p));

        mockMvc.perform(MockMvcRequestBuilders.get("/pessoa3s/{pessoa3Id}", pessoa3Id))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}
